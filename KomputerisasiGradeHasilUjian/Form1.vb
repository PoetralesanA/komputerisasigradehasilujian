'///////////////////////////////////////
'Coders : PoetralesanA
'Gitlab : gitlab.com/PoetralesanA
'Email : Poetralesana.vbproj@gmail.com
'///////////////////////////////////////

Public Class Form1
    Private Declare Function HideCaret Lib "user32.dll" (ByVal hWnd As IntPtr) As Boolean



    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtNamaSiswa.Text = "BrokecoderZ"
    End Sub
    Private Sub CheckingResult(sender As Object, e As EventArgs) Handles btnCheck.Click
        'variable untuk mengambil data
        Dim getData As Object = String.Empty

        'mengambil data pada module AlgoritmaGrade
        getData = AlgoritmaGrade.rumus(
                            txtNilaiAkhir.Text,
                            txtNilaiPraktik.Text,
                            txtTeori.Text
                            )

        'getData(0) = mengambil hasil KETERANGAN pada module AlgoritmaGrade
        'getData(1) = mengambil hasil NILAI AKHIR pada module AlgoritmaGrade
        txtKeterangan.Text = getData(0)
        txtNilaiAkhir.Text = getData(1)
    End Sub













#Region "Mengaktifkan fungsi numberic only"
    Private Sub txtNilaiPraktik_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNilaiPraktik.KeyPress
        AktifkanNumbericOnlye(e)
    End Sub
    Private Sub txtTeori_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTeori.KeyPress
        AktifkanNumbericOnlye(e)
    End Sub
    Private Sub txtNilaiAkhir_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNilaiAkhir.KeyPress
        AktifkanNumbericOnlye(e)
    End Sub
    Private Sub AktifkanNumbericOnlye(e As KeyPressEventArgs)
        If Not IsNumeric(e.KeyChar) And Not e.KeyChar = ChrW(Keys.Back) Then
            e.Handled = True
        End If
    End Sub
#End Region
#Region "DisableIBeam/Mematikan fungsi Cursor garis"
    Private Sub disableIBnilaiAkhir(sender As Object, e As EventArgs) Handles txtNilaiAkhir.GotFocus
        HideCaret(txtNilaiAkhir.Handle)
    End Sub
    Private Sub disableIBketerangan(sender As Object, e As EventArgs) Handles txtKeterangan.GotFocus
        HideCaret(txtKeterangan.Handle)
    End Sub
#End Region

End Class
