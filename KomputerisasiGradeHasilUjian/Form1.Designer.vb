<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtKeterangan = New System.Windows.Forms.TextBox()
        Me.txtNilaiAkhir = New System.Windows.Forms.TextBox()
        Me.txtTeori = New System.Windows.Forms.TextBox()
        Me.txtNilaiPraktik = New System.Windows.Forms.TextBox()
        Me.txtNamaSiswa = New System.Windows.Forms.TextBox()
        Me.txtNoAbsen = New System.Windows.Forms.TextBox()
        Me.btnCheck = New System.Windows.Forms.Button()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(154, 25)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(176, 138)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(15, 39)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(90, 19)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "No. Absen"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(15, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(99, 19)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Nama Siswa"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(15, 117)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(126, 19)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Nilai Praktik"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(15, 160)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(108, 19)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Nilai Teori"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(15, 199)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(108, 19)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Nilai Akhir"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(15, 233)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(99, 19)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Keterangan"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtKeterangan)
        Me.GroupBox1.Controls.Add(Me.txtNilaiAkhir)
        Me.GroupBox1.Controls.Add(Me.txtTeori)
        Me.GroupBox1.Controls.Add(Me.txtNilaiPraktik)
        Me.GroupBox1.Controls.Add(Me.txtNamaSiswa)
        Me.GroupBox1.Controls.Add(Me.txtNoAbsen)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 213)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(463, 295)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Check Nilai"
        '
        'txtKeterangan
        '
        Me.txtKeterangan.BackColor = System.Drawing.Color.Black
        Me.txtKeterangan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtKeterangan.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtKeterangan.ForeColor = System.Drawing.SystemColors.Info
        Me.txtKeterangan.Location = New System.Drawing.Point(176, 234)
        Me.txtKeterangan.MaxLength = 0
        Me.txtKeterangan.Name = "txtKeterangan"
        Me.txtKeterangan.ReadOnly = True
        Me.txtKeterangan.Size = New System.Drawing.Size(185, 22)
        Me.txtKeterangan.TabIndex = 12
        '
        'txtNilaiAkhir
        '
        Me.txtNilaiAkhir.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtNilaiAkhir.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNilaiAkhir.ForeColor = System.Drawing.Color.White
        Me.txtNilaiAkhir.Location = New System.Drawing.Point(176, 200)
        Me.txtNilaiAkhir.MaxLength = 0
        Me.txtNilaiAkhir.Name = "txtNilaiAkhir"
        Me.txtNilaiAkhir.ReadOnly = True
        Me.txtNilaiAkhir.Size = New System.Drawing.Size(240, 22)
        Me.txtNilaiAkhir.TabIndex = 11
        '
        'txtTeori
        '
        Me.txtTeori.Location = New System.Drawing.Point(176, 161)
        Me.txtTeori.Name = "txtTeori"
        Me.txtTeori.Size = New System.Drawing.Size(240, 22)
        Me.txtTeori.TabIndex = 10
        '
        'txtNilaiPraktik
        '
        Me.txtNilaiPraktik.Location = New System.Drawing.Point(176, 118)
        Me.txtNilaiPraktik.Name = "txtNilaiPraktik"
        Me.txtNilaiPraktik.Size = New System.Drawing.Size(240, 22)
        Me.txtNilaiPraktik.TabIndex = 9
        '
        'txtNamaSiswa
        '
        Me.txtNamaSiswa.Location = New System.Drawing.Point(176, 76)
        Me.txtNamaSiswa.Name = "txtNamaSiswa"
        Me.txtNamaSiswa.Size = New System.Drawing.Size(240, 22)
        Me.txtNamaSiswa.TabIndex = 8
        '
        'txtNoAbsen
        '
        Me.txtNoAbsen.Location = New System.Drawing.Point(176, 40)
        Me.txtNoAbsen.Name = "txtNoAbsen"
        Me.txtNoAbsen.Size = New System.Drawing.Size(163, 22)
        Me.txtNoAbsen.TabIndex = 7
        '
        'btnCheck
        '
        Me.btnCheck.Location = New System.Drawing.Point(273, 524)
        Me.btnCheck.Name = "btnCheck"
        Me.btnCheck.Size = New System.Drawing.Size(188, 61)
        Me.btnCheck.TabIndex = 13
        Me.btnCheck.Text = "Check>>"
        Me.btnCheck.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(483, 608)
        Me.Controls.Add(Me.btnCheck)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "Form1"
        Me.Opacity = 0.96R
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Komputerisasi Grade Hasil Ujian"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtKeterangan As System.Windows.Forms.TextBox
    Friend WithEvents txtNilaiAkhir As System.Windows.Forms.TextBox
    Friend WithEvents txtTeori As System.Windows.Forms.TextBox
    Friend WithEvents txtNilaiPraktik As System.Windows.Forms.TextBox
    Friend WithEvents txtNamaSiswa As System.Windows.Forms.TextBox
    Friend WithEvents txtNoAbsen As System.Windows.Forms.TextBox
    Friend WithEvents btnCheck As System.Windows.Forms.Button

End Class
