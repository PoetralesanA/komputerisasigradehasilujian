'///////////////////////////////////////
'Coders : PoetralesanA
'Gitlab : gitlab.com/PoetralesanA
'Email : Poetralesana.vbproj@gmail.com
'///////////////////////////////////////

Module AlgoritmaGrade
    Public Function rumus(nilaiakhir, nilaipraktik, nilaiteori)
        Dim keterangan = String.Empty
        Try
            'Rumus
            nilaiakhir = 0.6 * nilaipraktik + 0.4 * nilaiteori

            'Check result Keterangan
            If nilaiakhir < 60 Then
                keterangan = "Kurang"
            ElseIf nilaiakhir >= 60 And nilaiakhir < 75 Then
                keterangan = "Cukup"
            ElseIf nilaiakhir >= 75 Then
                keterangan = "Baik"
            End If
        Catch ex As Exception
            MsgBox("Data Yang Anda Masukan Tidak Lengkap!")
        End Try
        Return {keterangan, nilaiakhir}
    End Function

End Module
